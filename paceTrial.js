let arr = [];
for(let i =0 ; i<2000000; i++){
    let randNum  = Math.floor(Math.random()*1000000);
    arr[i] = randNum;
}
console.time("duration for array");
console.time(arr.indexOf(789523));
console.timeEnd("duration for array");

let mySet = new Set();
for(let i =0 ; i<2000000; i++){
    let randNum  = Math.floor(Math.random()*100000);
    mySet.add(randNum);
}
console.time("duration for set");
console.time(mySet.has(222333));
console.timeEnd("duration for set");

let myMap = new Map();
for(let i =0 ; i<2000000; i++){
    let randNum1  = Math.floor(Math.random()*1000000);
    let randNum2  = Math.floor(Math.random()*1000000);
    myMap[i] = true;
}

console.time("duration for map with hasOwnProperty");
console.time(myMap.hasOwnProperty(99003));
console.timeEnd("duration for map with hasOwnProperty");
console.time("duration for map");
console.time(myMap[99003]);
console.timeEnd("duration for map");

